#include <endian.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <sys/time.h>

#define calcIndex(width, x, y)  ((y)*(width) + (x))

long TimeSteps = 100;

void writeVTK2(long timestep, double *data, char prefix[1024], long w, long h) {
    char filename[2048];
    int x, y;

    long offsetX = 0;
    long offsetY = 0;
    float deltax = 1.0;
    float deltay = 1.0;
    long nxy = w * h * sizeof(float);

    snprintf(filename, sizeof(filename), "%s-%05ld%s", prefix, timestep, ".vti");
    FILE *fp = fopen(filename, "w");

    fprintf(fp, "<?xml version=\"1.0\"?>\n");
    fprintf(fp, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n");
    fprintf(fp, "<ImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n", offsetX,
            offsetX + w - 1, offsetY, offsetY + h - 1, 0, 0, deltax, deltax, 0.0);
    fprintf(fp, "<CellData Scalars=\"%s\">\n", prefix);
    fprintf(fp, "<DataArray type=\"Float32\" Name=\"%s\" format=\"appended\" offset=\"0\"/>\n", prefix);
    fprintf(fp, "</CellData>\n");
    fprintf(fp, "</ImageData>\n");
    fprintf(fp, "<AppendedData encoding=\"raw\">\n");
    fprintf(fp, "_");
    fwrite((unsigned char *) &nxy, sizeof(long), 1, fp);

    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            float value = data[calcIndex(h, x, y)];
            fwrite((unsigned char *) &value, sizeof(float), 1, fp);
        }
    }

    fprintf(fp, "\n</AppendedData>\n");
    fprintf(fp, "</VTKFile>\n");
    fclose(fp);
}


void show(double *currentfield, int w, int h) {
    printf("\033[H");
    int x, y;
    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) printf(currentfield[calcIndex(w, x, y)] ? "\033[07m  \033[m" : "  ");
        //printf("\033[E");
        printf("\n");
    }
    fflush(stdout);
}

int calcNeighbours(double *currentfield, int x, int y, int width) {
    int neighbours = -currentfield[calcIndex(width, x, y)];
    for (int i = x - 1; i <= x + 1; i++) {
        for (int j = y - 1; j <= y + 1; j++) {
            neighbours += currentfield[calcIndex(width, i, j)];
        }
    }
    return neighbours;
}

void evolve(double *currentfield, double *newfield, int w, int h) {
    int x, y;

    int tid = omp_get_thread_num();

    int xStart, xEnd, yStart, yEnd;

    /*
    xStart = (tid % 2) * w / 2;
    xEnd = (tid % 2 +1) * w / 2;
    yStart= (tid % 2) * h / 2;
    yEnd = (tid % 2 +1) * w / 2;
    printf("Thread with %d has %d, %d, %d, %d \n", tid, xStart, xEnd, yStart, yEnd);
    */
    if (tid == 0) {
        xStart = 1;
        xEnd = (w - 1) / 2;
        yStart= 1;
        yEnd = (h - 1) / 2;
    } else if (tid == 1) {
        xStart = 1;
        xEnd = (w - 1) / 2;
        yStart = (h - 1) / 2;
        yEnd = h - 2;
    }else if (tid == 2) {
        xStart = (w - 1) / 2;
        xEnd = w -2;
        yStart= 1;
        yEnd = (h - 1) / 2;
    }else if (tid == 3) {
        xStart = (w - 1) / 2;
        xEnd = w -2;
        yStart= (h - 1) / 2;
        yEnd = h - 2;
    }
    //printf("Thread with %d has %d, %d, %d, %d \n", tid, xStart, xEnd, yStart, yEnd);

    for (y = yStart; y < yEnd; y++) {
        for (x = xStart; x < xEnd; x++) {
            int neighbours = calcNeighbours(currentfield, x, y, w);
            if (currentfield[calcIndex(w, x, y)] == 0) {
                if (neighbours == 3) {
                    newfield[calcIndex(w, x, y)] = 1;
                } else {
                    newfield[calcIndex(w, x, y)] = 0;
                }
            } else {
                if (neighbours == 2 || neighbours == 3) {
                    newfield[calcIndex(w, x, y)] = 1;
                } else {
                    newfield[calcIndex(w, x, y)] = 0;
                }
            }
        }
    }
}

void filling(double *currentfield, int w, int h) {
    int i;
    for (i = 0; i < h * w; i++) {
        currentfield[i] = (rand() < RAND_MAX / 10) ? 1 : 0; ///< init domain randomly
    }
//    currentfield[calcIndex(w, 5, 28)] = 1;
//    currentfield[calcIndex(w, 5, 1)] = 1;
//    currentfield[calcIndex(w, 5, 2)] = 1;
//
//    currentfield[calcIndex(w, 28, 5)] = 1;
//    currentfield[calcIndex(w, 1, 5)] = 1;
//    currentfield[calcIndex(w, 2, 5)] = 1;
//
//    currentfield[calcIndex(w, 13, 20)] = 1;
//    currentfield[calcIndex(w, 13, 19)] = 1;
//    currentfield[calcIndex(w, 13, 18)] = 1;

}

void game(int w, int h) {
    double *currentfield = calloc(w * h, sizeof(double));
    double *newfield = calloc(w * h, sizeof(double));

    //printf("size unsigned %d, size long %d\n",sizeof(float), sizeof(long));

    filling(currentfield, w, h);
    long t;
    for (t = 0; t < TimeSteps; t++) {
#pragma omp parallel sections num_threads(2)
        {
#pragma omp section
            {
                int x;
                // Ghost Bottom auf Top
                for (x = 1; x < w - 2; x++) {
                    currentfield[calcIndex(w, x, 0)] = currentfield[calcIndex(w, x, h - 2)];
                    currentfield[calcIndex(w, x, h - 1)] = currentfield[calcIndex(w, x, 1)];
                }
            }
#pragma omp section
            {
                int y;
                // Ghost Right auf Left
                for (y = 1; y < h - 2; y++) {
                    currentfield[calcIndex(w, 0, y)] = currentfield[calcIndex(w, w - 2, y)];
                    currentfield[calcIndex(w, w - 1, y)] = currentfield[calcIndex(w, 1, y)];
                }
            }
        }

#pragma omp parallel num_threads(4)
        {
            evolve(currentfield, newfield, w, h);
        }
        show(currentfield, w, h);

        printf("%ld timestep\n", t);
        writeVTK2(t, currentfield, "gol", w, h);

        usleep(200000);

        //SWAP
        double *temp = currentfield;
        currentfield = newfield;
        newfield = temp;
    }

    free(currentfield);
    free(newfield);

}

int main(int c, char **v) {
    int w = 0, h = 0;
    if (c > 1) w = atoi(v[1]); ///< read width
    if (c > 2) h = atoi(v[2]); ///< read height
    if (w <= 0) w = 30; ///< default width
    if (h <= 0) h = 30; ///< default height
    game(w, h);
}
